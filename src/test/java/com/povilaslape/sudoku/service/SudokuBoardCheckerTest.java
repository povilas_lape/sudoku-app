package com.povilaslape.sudoku.service;

import org.junit.Test;

import java.util.List;
import java.util.function.Function;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

public class SudokuBoardCheckerTest {

  private final SudokuBoardChecker sudokuBoardChecker = new SudokuBoardChecker();
  private final SudokuValidators sudokuValidators = new SudokuValidators();

  private final static Function<List<List<Integer>>, Boolean> NO_CHECK_FUNCTION = (board) -> true;

  @Test
  public void returnTrueWhenAllRowsAreValid() {
    var validRow = List.of(List.of(1, 2, 3, 4, 5, 6, 7, 8, 9));
    var result = sudokuBoardChecker.isSudokuBoardValid(validRow, sudokuValidators.rowsValidator, NO_CHECK_FUNCTION,
        NO_CHECK_FUNCTION);

    assertThat(result).isTrue();
  }

  @Test
  public void returnFalseRowContainsDuplicates() {
    var row = List.of(List.of(1, 2, 3, 4, 5, 6, 7, 8, 1));
    var result = sudokuBoardChecker.isSudokuBoardValid(row, sudokuValidators.rowsValidator, NO_CHECK_FUNCTION,
        NO_CHECK_FUNCTION);

    assertThat(result).isFalse();
  }

  @Test
  public void returnFalseRowContainsLessThanNineElements() {
    var row = List.of(List.of(1, 2, 3, 4, 5, 6, 7, 8));
    var result = sudokuBoardChecker.isSudokuBoardValid(row, sudokuValidators.rowsValidator, NO_CHECK_FUNCTION,
        NO_CHECK_FUNCTION);

    assertThat(result).isFalse();
  }

  @Test
  public void returnFalseRowContainsLessMoreThanNineElements() {
    var row = List.of(List.of(1, 2, 3, 4, 5, 6, 7, 8, 9, 10));
    var result = sudokuBoardChecker.isSudokuBoardValid(row, sudokuValidators.rowsValidator, NO_CHECK_FUNCTION,
        NO_CHECK_FUNCTION);

    assertThat(result).isFalse();
  }

  @Test
  public void returnFalseWhenJustOneRowValidGivenMultipleRows() {
    var row = List.of(List.of(1, 2, 3, 4, 5, 6, 7, 8, 9), List.of(1, 1, 1, 1, 1, 1, 1, 1, 1));
    var result = sudokuBoardChecker.isSudokuBoardValid(row, sudokuValidators.rowsValidator, NO_CHECK_FUNCTION,
        NO_CHECK_FUNCTION);

    assertThat(result).isFalse();
  }

  @Test
  public void returnFalseOnEmptyRow() {
    List<List<Integer>> emptyRow = List.of(List.of());
    var result = sudokuBoardChecker.isSudokuBoardValid(emptyRow, sudokuValidators.rowsValidator, NO_CHECK_FUNCTION,
        NO_CHECK_FUNCTION);

    assertThat(result).isFalse();
  }

  @Test
  public void returnTrueWhenAllColumnsAreValid() {
    var rows = List.of(
        List.of(1, 1),
        List.of(2, 2),
        List.of(3, 3),
        List.of(4, 4),
        List.of(5, 5),
        List.of(6, 6),
        List.of(7, 7),
        List.of(8, 8),
        List.of(9, 9)
    );

    var result = sudokuBoardChecker.isSudokuBoardValid(rows, NO_CHECK_FUNCTION, sudokuValidators.columnsValidator,
        NO_CHECK_FUNCTION);

    assertThat(result).isTrue();
  }

  @Test
  public void returnFalseWhenColumnContainsMoreThanNineElements() {
    var rows = List.of(
        List.of(1),
        List.of(1),
        List.of(3),
        List.of(4),
        List.of(5),
        List.of(6),
        List.of(7),
        List.of(8),
        List.of(9),
        List.of(10)
    );

    var result = sudokuBoardChecker.isSudokuBoardValid(rows, NO_CHECK_FUNCTION, sudokuValidators.columnsValidator,
        NO_CHECK_FUNCTION);

    assertThat(result).isFalse();
  }

  @Test
  public void returnFalseWhenColumnContainsDuplicates() {
    var rows = List.of(
        List.of(1, 1),
        List.of(1, 2),
        List.of(3, 3),
        List.of(4, 4),
        List.of(5, 5),
        List.of(6, 6),
        List.of(7, 7),
        List.of(8, 8),
        List.of(9, 9)
    );

    var result = sudokuBoardChecker.isSudokuBoardValid(rows, NO_CHECK_FUNCTION, sudokuValidators.columnsValidator,
        NO_CHECK_FUNCTION);

    assertThat(result).isFalse();
  }

  @Test
  public void returnFalseWhenColumnContainsLessThanNineElements() {
    var rows = List.of(
        List.of(1, 1),
        List.of(1, 2),
        List.of(3, 3),
        List.of(4, 4),
        List.of(5, 5),
        List.of(6, 6),
        List.of(7, 7),
        List.of(8, 8)
    );

    var result = sudokuBoardChecker.isSudokuBoardValid(rows, NO_CHECK_FUNCTION, sudokuValidators.columnsValidator,
        NO_CHECK_FUNCTION);

    assertThat(result).isFalse();
  }

  @Test
  public void returnTrueWhenSquaresAreValid() {
    var rows = List.of(
        List.of(1, 2, 3, 1, 2, 3, 1, 2, 3),
        List.of(4, 5, 6, 4, 5, 6, 4, 5, 6),
        List.of(7, 8, 9, 7, 8, 9, 7, 8, 9),
        List.of(1, 2, 3, 1, 2, 3, 1, 2, 3),
        List.of(4, 5, 6, 4, 5, 6, 4, 5, 6),
        List.of(7, 8, 9, 7, 8, 9, 7, 8, 9),
        List.of(1, 2, 3, 1, 2, 3, 1, 2, 3),
        List.of(4, 5, 6, 4, 5, 6, 4, 5, 6),
        List.of(7, 8, 9, 7, 8, 9, 7, 8, 9)
    );

    var result = sudokuBoardChecker.isSudokuBoardValid(rows, NO_CHECK_FUNCTION, NO_CHECK_FUNCTION,
        sudokuValidators.squaresValidator);

    assertThat(result).isTrue();
  }

  @Test
  public void returnFalseWhenAtLeastOneSquareIsInvalid() {
    var rows = List.of(
        List.of(1, 1, 3, 1, 2, 3, 1, 2, 3),
        List.of(4, 5, 6, 4, 5, 6, 4, 5, 6),
        List.of(7, 8, 9, 7, 8, 9, 7, 8, 9),
        List.of(1, 2, 3, 1, 2, 3, 1, 2, 3),
        List.of(4, 5, 6, 4, 5, 6, 4, 5, 6),
        List.of(7, 8, 9, 7, 8, 9, 7, 8, 9),
        List.of(1, 2, 3, 1, 2, 3, 1, 2, 3),
        List.of(4, 5, 6, 4, 5, 6, 4, 5, 6),
        List.of(7, 8, 9, 7, 8, 9, 7, 8, 9)
    );

    var result = sudokuBoardChecker.isSudokuBoardValid(rows, NO_CHECK_FUNCTION, NO_CHECK_FUNCTION,
        sudokuValidators.squaresValidator);

    assertThat(result).isFalse();
  }

  @Test
  public void returnFalseWhenNotFullSudokuGrid() {
    var rows = List.of(
        List.of(1, 1, 3, 1, 2, 3),
        List.of(4, 5, 6, 4, 5, 6),
        List.of(7, 8, 9, 7, 8, 9),
        List.of(1, 2, 3, 1, 2, 3),
        List.of(4, 5, 6, 4, 5, 6),
        List.of(7, 8, 9, 7, 8, 9),
        List.of(1, 2, 3, 1, 2, 3),
        List.of(4, 5, 6, 4, 5, 6),
        List.of(7, 8, 9, 7, 8, 9)
    );

    var result = sudokuBoardChecker.isSudokuBoardValid(rows, NO_CHECK_FUNCTION, NO_CHECK_FUNCTION,
        sudokuValidators.squaresValidator);

    assertThat(result).isFalse();
  }

  @Test
  public void failWhenPassedGridIsEmpty() {
    List<List<Integer>> rows = List.of();

    assertThatThrownBy(() -> {
      sudokuBoardChecker.isSudokuBoardValid(rows, NO_CHECK_FUNCTION, NO_CHECK_FUNCTION,
          sudokuValidators.squaresValidator);
    }).isInstanceOf(IllegalArgumentException.class);
  }

  @Test
  public void returnTrueWhenSquaresRowsAndColumnsAreValid() {
    var rows = List.of(
        List.of(7, 2, 6, 4, 9, 3, 8, 1, 5),
        List.of(3, 1, 5, 7, 2, 8, 9, 4, 6),
        List.of(4, 8, 9, 6, 5, 1, 2, 3, 7),
        List.of(8, 5, 2, 1, 4, 7, 6, 9, 3),
        List.of(6, 7, 3, 9, 8, 5, 1, 2, 4),
        List.of(9, 4, 1, 3, 6, 2, 7, 5, 8),
        List.of(1, 9, 4, 8, 3, 6, 5, 7, 2),
        List.of(5, 6, 7, 2, 1, 4, 3, 8, 9),
        List.of(2, 3, 8, 5, 7, 9, 4, 6, 1)
    );

    var result =
        sudokuBoardChecker.isSudokuBoardValid(rows, sudokuValidators.rowsValidator, sudokuValidators.columnsValidator,
            sudokuValidators.squaresValidator);

    assertThat(result).isTrue();
  }

}
