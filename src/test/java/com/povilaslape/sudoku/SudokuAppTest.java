package com.povilaslape.sudoku;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Objects;

import static org.assertj.core.api.Assertions.assertThat;

public class SudokuAppTest {

  private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();

  @Before
  public void setUpStream() {
    System.setOut(new PrintStream(outContent));
  }

  @After
  public void cleanUpStream() {
    System.setOut(null);
  }

  @Test
  public void outputValidWhenSudokuBoardIsValid() {
    final var filePath = getInputFilePath("input/valid_board.csv");
    SudokuApp.main(new String[]{ filePath });

    assertThat(outContent.toString()).isEqualTo("VALID");
  }

  @Test
  public void outputInvalidWhenSudokuBoardIsNotAccordingToRules() {
    final var filePath = getInputFilePath("input/invalid_board_not_according_to_rules.csv");
    SudokuApp.main(new String[]{ filePath });

    assertThat(outContent.toString()).contains("INVALID");
    assertThat(outContent.toString()).contains("Warning: Provided Sudoku board is not according to the rules");
  }

  @Test
  public void outputInvalidWhenSudokuBoardIsNotFullGrid() {
    final var filePath = getInputFilePath("input/invalid_board_not_full.csv");
    SudokuApp.main(new String[]{ filePath });

    assertThat(outContent.toString()).contains("INVALID");
    assertThat(outContent.toString()).contains("Warning: Provided Sudoku board is not according to the rules");
  }

  @Test
  public void outputInvalidWhenSudokuBoardFileFailsToBeRead() {
    final var filePath = getInputFilePath("input/invalid_board_non_numbers.csv");
    SudokuApp.main(new String[]{ filePath });

    assertThat(outContent.toString()).contains("INVALID");
    assertThat(outContent.toString()).contains("Error: Invalid file content");
  }

  @Test
  public void outputInvalidWhenSudokuBoardFileFailsToBeReadEmpty() {
    final var filePath = getInputFilePath("input/invalid_board_empty.csv");
    SudokuApp.main(new String[]{ filePath });

    assertThat(outContent.toString()).contains("INVALID");
    assertThat(outContent.toString()).contains("Error: Invalid file content: Board is empty");
  }

  @Test
  public void outputErrorWhenFileNotSpecified() {
    SudokuApp.main(new String[]{});

    assertThat(outContent.toString()).contains("INVALID");
    assertThat(outContent.toString()).contains("Error: File name not specified");
  }

  @Test
  public void outputErrorWhenFileNotExists() {
    SudokuApp.main(new String[]{"/bla.txt"});

    assertThat(outContent.toString()).contains("INVALID");
    assertThat(outContent.toString()).contains("Failure to read the file: /bla.txt");
  }

  private String getInputFilePath(String resourcePath) {
    return Objects.requireNonNull(getClass().getClassLoader().getResource(resourcePath)).getFile();
  }

}
