package com.povilaslape.sudoku;

import java.io.IOException;

public class SudokuApp {

  private final static Wiring wiring = new Wiring();

  public static void main(String[] args) {

    if (args.length == 0) {
      System.out.println("INVALID");
      System.out.print("Error: File name not specified");
    } else {
      try {
        final var sudokuBoard = wiring.sudokuBoardReader.readCVSBoard(args[0]);

        final var isValid = wiring.sudokuBoardChecker.isSudokuBoardValid(
            sudokuBoard,
            wiring.sudokuValidators.rowsValidator,
            wiring.sudokuValidators.columnsValidator,
            wiring.sudokuValidators.columnsValidator);

        if (isValid) {
          System.out.print("VALID");
        } else {
          System.out.println("INVALID");
          System.out.print("Warning: Provided Sudoku board is not according to the rules");
        }
      } catch (IOException ex) {
        System.out.println("INVALID");
        System.out.printf("Error: Failure to read the file: %s", ex.getLocalizedMessage());
      }
      catch (IllegalArgumentException ex) {
        System.out.println("INVALID");
        System.out.printf("Error: Invalid file content: %s", ex.getLocalizedMessage());
      }
    }
  }

}