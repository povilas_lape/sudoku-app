package com.povilaslape.sudoku.service;

import java.util.List;
import java.util.function.Function;

public class SudokuBoardChecker {

  public boolean isSudokuBoardValid(
      List<List<Integer>> board,
      Function<List<List<Integer>>, Boolean> rowsValidatorFn,
      Function<List<List<Integer>>, Boolean> columnsValidatorFn,
      Function<List<List<Integer>>, Boolean> squaresValidatorFn) {

    if (board.isEmpty()) {
      throw new IllegalArgumentException("Board is empty");
    }

    final var rowsValid = rowsValidatorFn.apply(board);
    final var columnsValid = columnsValidatorFn.apply(board);
    final var squaresValid = squaresValidatorFn.apply(board);

    return rowsValid && columnsValid && squaresValid;
  }

}
