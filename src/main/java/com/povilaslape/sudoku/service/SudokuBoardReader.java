package com.povilaslape.sudoku.service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class SudokuBoardReader {

  public List<List<Integer>> readCVSBoard(String filePath) throws IOException {
    try {
      var lines = Files.lines(Paths.get(filePath));

      List<List<Integer>> inputLists =
          lines
              .map(line ->
                  Arrays.stream(line.split(","))
                      .map(Integer::parseInt)
                      .collect(Collectors.toList())
              ).collect(Collectors.toList());

      lines.close();
      return inputLists;
    } catch (NumberFormatException ex) {
      throw new IllegalArgumentException("File is mis-formatted", ex);
    }
  }

}
