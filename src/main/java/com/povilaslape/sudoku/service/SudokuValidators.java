package com.povilaslape.sudoku.service;

import java.util.HashSet;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class SudokuValidators {

  public final Function<List<List<Integer>>, Boolean> rowsValidator =
      (board) -> board.stream().allMatch(this::isValidGroup);

  public final Function<List<List<Integer>>, Boolean> columnsValidator =
      (board) -> transpose(board).stream().allMatch(this::isValidGroup);

  public final Function<List<List<Integer>>, Boolean> squaresValidator =
      (board) -> group(board, 3)
          .stream()
          .map(this::transpose)
          .flatMap(transposed -> group(transposed, 3).stream())
          .map(s -> s.stream().flatMap(List::stream).collect(Collectors.toList()))
          .allMatch(this::isValidGroup);

  private List<List<List<Integer>>> group(List<List<Integer>> originalList, Integer groupLength) {
    return IntStream.range(0, groupLength)
        .mapToObj(
            i -> originalList.subList(groupLength * i, Math.min(groupLength + (groupLength * i), originalList.size())))
        .collect(Collectors.toList());
  }

  private List<List<Integer>> transpose(List<List<Integer>> board) {
    final var newRowLength = board.stream().mapToInt(List::size).max().orElse(0);

    return IntStream.range(0, newRowLength)
        .mapToObj(index ->
            board
                .stream()
                .map(list -> list.get(index))
                .collect(Collectors.toList())
        )
        .collect(Collectors.toList());
  }

  private boolean isValidGroup(List<Integer> group) {
    final var range = IntStream.rangeClosed(1, 9).boxed().collect(Collectors.toSet());
    final var inputSet = new HashSet<>(group);

    final var hasNoDuplicates = inputSet
        .stream()
        .allMatch(groupNum -> range.stream().anyMatch(groupNum::equals));

    final var hasCorrectLength = inputSet.size() == range.size();

    return hasNoDuplicates && hasCorrectLength;
  }
}
