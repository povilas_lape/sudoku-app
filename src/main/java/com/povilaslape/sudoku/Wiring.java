package com.povilaslape.sudoku;

import com.povilaslape.sudoku.service.SudokuBoardReader;
import com.povilaslape.sudoku.service.SudokuBoardChecker;
import com.povilaslape.sudoku.service.SudokuValidators;

public class Wiring {

  final SudokuBoardReader sudokuBoardReader = new SudokuBoardReader();
  final SudokuValidators sudokuValidators = new SudokuValidators();
  final SudokuBoardChecker sudokuBoardChecker = new SudokuBoardChecker();

}
