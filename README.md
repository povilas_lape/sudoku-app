# sudoku-app

This repository is for the sudoku validator app.

Java 11 is needed to run and build the app. To run the tests run `mvn test`.

In order to run the app on MAC OS need to execute `mvn package` to package JAR.
JAR will be packed as `sudoku-app-1.0-SNAPSHOT-jar-with-dependencies.jar` under
`/target` directory.

To run the jar: `java -jar  target/sudoku-app-1.0-SNAPSHOT-jar-with-dependencies.jar /path/to/sudoku/file.csv`

To generate surefire report: `mvn surefire-report:report`